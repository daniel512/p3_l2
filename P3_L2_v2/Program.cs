﻿using System;

namespace P3_L2_v2
{
    class Program
    {
        static int[] counters = { 0, 0, 0 };
        static int counter = 0;
        static int target = 6;
        static void Label(int index)
        {
            Console.WriteLine("Label " + counters[index]);
            counters[index - 1]++;
        }

        static int Sum()
        {

            int sum = 0;
            foreach (var c in counters)
            {
                sum += c;
            }
            return sum;

        }
        static void Main(string[] args)
        {

            string text = Console.ReadLine();

            while (Sum() != target - 1)
            {
                switch (text)
                {
                    case "1":
                        Label(1);
                        break;
                    case "2":
                        Label(2);
                        break;
                    case "3":
                        Label(3);
                        break;

                    default:
                        break;
                }
            }

        }
    }
}
